set title 'DEPTH, ROLL, YAW'

set xlabel 'depth'
set ylabel 'roll'
set zlabel 'yaw'

splot name u 3:4:($7==2?$6:1/0) w p pt 7 lt 2 t 'aligned', name u 3:4:($7==1?$6:1/0) w p pt 7 lt 6 ps 0.75 t 'misaligned', name u 3:4:($7==0?$6:1/0) w p pt 7 lt 1 ps 0.1 t 'failed'
