set title 'RPY'

set xlabel 'roll'
set ylabel 'pitch'
set zlabel 'yaw'

splot name u 1:2:3 w p pt 1 lt 1 t 'R'
