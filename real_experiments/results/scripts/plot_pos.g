set title 'POSITION'

set xlabel 'x'
set ylabel 'y'
set zlabel 'z'

splot name u 1:2:($7==2?$3:1/0) w p pt 7 lt 2 t 'aligned', name u 1:2:($7==1?$3:1/0) w p pt 7 lt 6 ps 0.75 t 'misaligned', name u 1:2:($7==0?$3:1/0) w p pt 7 lt 1 ps 0.1 t 'failed'
