library('rgl')

# Configuration
PHI=25
THETA=25

# Read data
args <- commandArgs(T)
(data_file <- args[1])
(plot_file <- args[2])

vs <- read.csv(data_file)

# extract x versors
xs = split(vs, vs$a == 0)$`TRUE`
ys = split(vs, vs$a == 1)$`TRUE`
zs = split(vs, vs$a == 2)$`TRUE`

# plot success
pic <- plot3d(
	xs[,1], xs[,2], xs[,3], 
	col='red', axes=T,
	xlab='x', ylab='y', zlab='z'
)

pic <- plot3d(
	ys[,1], ys[,2], ys[,3], 
	col='green', axes=T,
	xlab='x', ylab='y', zlab='z',
	add=T
)

pic <- plot3d(
	zs[,1], zs[,2], zs[,3], 
	col='blue', axes=T,
	xlab='x', ylab='y', zlab='z',
	add=T
)

#axes3d(edges=c("x--", "y--", "z--"), box=T, expand=0.0, labels=T, tick=T)
view3d(theta=THETA, phi=PHI)
snapshot3d(plot_file)
