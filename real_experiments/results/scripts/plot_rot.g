set title 'ROTATION'

set xlabel 'roll'
set ylabel 'pitch'
set zlabel 'yaw'

splot name u 4:5:($7==2?$6:1/0) w p pt 7 lt 2 t 'aligned', name u 4:5:($7==1?$6:1/0) w p pt 7 lt 6 ps 0.75 t 'misaligned', name u 4:5:($7==0?$6:1/0) w p pt 7 lt 1 ps 0.1 t 'failed'
