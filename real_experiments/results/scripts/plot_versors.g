set title 'VERSORS'

set xlabel 'x'
set ylabel 'y'
set zlabel 'z'

set view equal xyz

splot name u 1:2:($4==0?$3:1/0) w p pt 1 lt 1 t 'x', name u 1:2:($4==1?$3:1/0) w p pt 1 lt 2 t 'y', name u 1:2:($4==2?$3:1/0) w p pt 1 lt 3 t 'z'

