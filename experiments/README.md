CONTENTS
========

doltspace1
----------
* Description: 
	Coverage 3d plot data on length / width / depth for dolt 1 scene
	
* Command:
	./gripper_space -c 4 -t 500 -r 0 -p 0 1 2 --dwc ~/grippers/scenes/dolt/Scene.dwc.xml --td ~/grippers/scenes/dolt/task1.td.xml -g ../data/grippers/flat.grp.xml -s ~/grippers/scenes/dolt/samples1.xml -o doltspace1 --res 10

* Comments:
	Width turns out not to influence coverage so much. Another experiment for length / depth / tcp is necessary.
	
cutspace3
---------
* Description:
	Alignment 3d plot data on cut parameters for rotor 3 scene

* Command:
	./gripper_space -c 4 -t 500 -r 0 -p 5 6 7 --dwc ~/grippers/scenes/rotor/Scene.dwc.xml --td ~/grippers/scenes/rotor/task3.td.xml -g ../data/grippers/flat.grp.xml -s ~/grippers/scenes/rotor/samples3.xml -o cutspace3 --res 10

* Comments:
	strannge peaks in data -- usable?

covspace
--------
* Description:
	3d plot on general dimensions for rotor scene (probably 1)

* Command:

* Comments:
	inconclusive

covspace1
---------
Description:
* 3d plot of coverage wrt length / depth / tcp offset for dolt 1 scene with a flat.grp.xml gripper
* I tinkered with alignment calculations in the duration of the experiments, so that result is not reliable
	
* Command:
	./gripper_space -c 4 -t 500 -r 0 -p 0 2 8 --dwc ~/grippers/scenes/dolt/Scene.dwc.xml --td ~/grippers/scenes/dolt/task1.td.xml -g ../data/grippers/flat.grp.xml -s ~/grippers/scenes/dolt/samples1.xml -o covspace1 --res 10

* Result:

test
----
Description:
* Testing whether changes in alignment broke things.
* rotor3 scene alignment cutspace

testslice
---------
Testing new slice tool that generates data for 2d parameter change.
This experiment is for cutdepth / cutangle test on rotor3 scene with n=100

cutspace
--------
Cut slice for all quality indices on flat.grp.xml.
Weird artifacts for bigger cut angles.
Paper grade?
