#!/bin/bash

GRP=$1

S=`xmllint --xpath '//*/success/text()' $GRP`
R=`xmllint --xpath '//*/robustness/text()' $GRP`
A=`xmllint --xpath '//*/alignment/text()' $GRP`
C=`xmllint --xpath '//*/coverage/text()' $GRP`
W=`xmllint --xpath '//*/wrench/text()' $GRP`
T=`xmllint --xpath '//*/stress/text()' $GRP`
V=`xmllint --xpath '//*/volume/text()' $GRP`
Q=`xmllint --xpath '//*/quality/text()' $GRP`

OUT=`basename $GRP .grp.xml`

echo "success, robustness, alignment, coverage, wrench, stress, volume, quality" > ${OUT}.csv
echo "${S}, ${R}, ${A}, ${C}, ${W}, ${T}, ${V}, ${Q}" >> ${OUT}.csv
